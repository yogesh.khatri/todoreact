import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import uniqid from 'uniqid'

// localStorage.setItem("listItems", JSON.stringify([]));

// creating list items
class ListItem extends React.Component {
    render() {
        return (
            <div className="list-item" position={this.props.position} >
                <input type="checkbox"
                    className="checkbox"
                    checked={this.props.checked}
                    onChange={this.props.onChange}
                ></input>
                <p
                    className="to-do-text"
                    style={{
                        textDecoration: this.props.checked ? "line-through" : "none",
                    }}
                >
                    {this.props.todo}

                </p>
                <button className="deleteBtn" onClick={this.props.onClick}>X</button>
            </div >
        )
    }
}

// creating list-items-conatainer
class ListContainer extends React.Component {
    state = {
        listItems: JSON.parse(window.localStorage.getItem("listItems")),
    }

    // function to delete and list-item
    deleteItem(e) {
        let pos = e.target.parentElement.getAttribute("position");
        let newList = this.state.listItems.filter((item) => {
            if (item["position"] != pos) {
                return item;
            }
        })
        this.setState({
            listItems: newList
        },
            updateLocalStorage(newList)
        )
    }

    // strike a item in list
    strikeItem(e) {
        let pos = e.target.parentElement.getAttribute("position");
        let checkboxVal = e.target.checked;
        let newList = this.state.listItems.map((item) => {
            if (item["position"] == pos) {
                item["checked"] = checkboxVal;
                return item;
            }
            return item;
        })
        this.setState({
            listItems: newList
        },
            updateLocalStorage(newList)
        )
    }

    // add new item in list
    addNewItem(e) {
        let pos = uniqid();
        e.preventDefault()
        let form = document.querySelector('form');
        let inputText = document.querySelector('.inputBox').value;
        let newList = this.state.listItems.slice();
        newList.unshift({ todo: inputText, checked: false, position: pos })
        pos += 1;
        this.setState({
            listItems: newList
        },
            updateLocalStorage(newList)
        )

        form.reset();
    }

    //funciton to render list-items 
    renderList(item) {
        // console.log(listItems)
        return <ListItem todo={item["todo"]}
            checked={item["checked"]}
            position={item["position"]}
            onChange={(e) => this.strikeItem(e)}
            onClick={(e) => this.deleteItem(e)}
        />;
    }
    // rendering Html in browser
    render() {
        return (
            <div className="main-container">
                <p className="heading">To Do List</p>
                <form>
                    <input type="text" className="inputBox"></input>
                    <button className="add-item-button" onClick={(e) => this.addNewItem(e)}>Add Item</button>
                </form>
                <div className="list-container">
                    {
                        this.state.listItems.map(item => (
                            this.renderList(item)
                        ))
                    }
                </div>
            </div>
        );
    }
}

// calling reactDOM
ReactDOM.render(
    <ListContainer />,
    document.getElementById('root')
);

function updateLocalStorage(listItems) {
    localStorage.setItem("listItems", JSON.stringify(listItems));
}
